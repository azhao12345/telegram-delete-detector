'use strict';
var TelegramClient = require('telegram-basic-bot');

var telegram = new TelegramClient('');

var datastore = {};

telegram.poll([]);

telegram.on('update', (update) => {
    //if (update.message && update.message.chat.id == -306012710) {
    //    return;
    //}
    if (update.message) {
        datastore[update.message.message_id] = update.message;

        // set timer to clear the data
        setTimeout(() => {
            datastore.delete(update.message.message_id);
        }, 1000 * 60 * 60 * 49);

        // set timer to check back for forwarding
        checkBackLater(update.message, 10000);
    } else if (update.edited_message) {
        console.log('edit detected-----------------------------------------');
        console.log(datastore[update.edited_message.message_id]);
        var origMessage = datastore[update.edited_message.message_id]
        telegram.request('sendMessage', {chat_id: origMessage.chat.id, text: getEditedMessage(origMessage.from.first_name) + '\n' + origMessage.text}, () => {});
    }
});

function checkBackLater(message, waitInterval) {
    setTimeout(() => {
        telegram.request('forwardMessage', {chat_id: '-306012710', from_chat_id: message.chat.id, disable_notification: true, message_id: message.message_id}, (err, result) => {
            //console.log('error ');
            //console.log(err);
            //console.log('result ');
            //console.log(result);
            var detected = false;
            if (err) {
                if (!err.indexOf) {
                    console.log('sheeeeeit strange errorg.  will retry');
                    console.log(err);
                } else if (err.indexOf('message to forward not found') != -1) {
                    console.log('deleted message detected------------------------------');
                    console.log(message);
                    detected = true;
                    telegram.request('sendMessage', {chat_id: message.chat.id, text: getDeletedMessage(message.from.first_name) + '\n' + message.text}, () => {});
                } else {
                    console.log('sheeeeeit unexpected errorg.  will retry');
                    console.log(err);
                }
            }
            if (!detected && waitInterval < 1000 * 60 * 60 * 48) {
                console.log('asking to check back later');
                checkBackLater(message, waitInterval * (1 + Math.random() * 0.3));
            }
            // TODO clean up
            if (result) {
                setTimeout(() => {
                    if (!result.message_id) {
                        console.log('wtf bad result ');
                        console.log(result);
                    }
                    telegram.request('deleteMessage', {chat_id: result.chat.id, message_id: result.message_id});
                }, 3000);
            }
        });
    }, waitInterval);
}

function getEditedMessage(name) {
    var messages = [
        'NAME changed his mind',
        'NAME tried to cover up his mistakes',
        'NAME was too high to type correctly',
        'did NAME really mean it?'
    ]
    return choose(messages).replace('NAME', name);
}

function getDeletedMessage(name) {
    var messages = [
        'The mob forced NAME to delete his tweet',
        'NAME ate his words',
        'NAME regrets sending that message',
        'diamonds are forever',
        'I could have sworn NAME said something like that'
    ]
    return choose(messages).replace('NAME', name);
}

function choose(items) {
    return items[Math.floor(Math.random()*items.length)];
}
